const express = require('express')
const mongoose = require('mongoose')

// Models
const Document = require('./models/Document')

const app = express()

mongoose.connect("mongodb://localhost:27017/HashBin", {
    useNewUrlParser: true,
    useUnifiedTopology: true
})

port = process.env.PORT || 5001

// View engine
app.set('view engine','ejs')
app.use(express.static('public'))
app.use(express.urlencoded({ extended: true }))

app.get('/', (req, res) => {
    const code = `Welcome to HashBin!

Use the commands in the top right corner
to create a new file to share with others.`

    res.render('code-display', { 
        code: code,
        language: 'plaintext'
    })
})

app.get('/new', (req, res) => {
    res.render('new')
})

app.post('/save', async (req, res) => {
    const value = req.body.value

    try {
        const document = await Document.create({ value })
        res.redirect(`/${document.id}`)
    } catch (error) {
        res.render("new", { value })
    }

})

app.get('/:id/duplicate', async (req, res) => {

    const id = req.params.id

    try {
        const document = await Document.findById(id)

        res.render("new", { value: document.value })
    } catch (error) {
        res.redirect(`/${id}`)
    }

})

app.get('/:id', async (req, res) => {
    const id = req.params.id

    try {
        const document = await Document.findById(id)

        res.render('code-display', { code: document.value, id })

    } catch (error) {
        res.redirect('/')
    }

})

app.listen (port, () => {
    console.log(`Server running on port ${port}`)
})